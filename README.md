# 008b - APP

Resources for **van der Linden, L.**, & Vitu, F. (accepted for publication). On the optimal viewing position for object processing. *Attention, Perception, & Psychophysics.*

## Data

Raw EDF files (compressed as ZIP files):

- `/edf`

Raw sound files (compressed from .wav to .mp3):

- `/voice`

## Stimuli

Only the word stimuli, because the license of Rossion and Pourtois' (2004) stimulus set is unknown.

- `/stimuli`

## Experimental script:

- `008b.osexp`

## License and credits

- Experimental scripts are available under the [GNU General Public License v3](http://www.gnu.org/licenses/gpl.html).
- Raw pictures are taken from:
	- the [Revised Snodgrass and Vanderwart object pictorial set](http://wiki.cnbc.cmu.edu/Objects), available under a [GNU Free Documentation License](http://www.gnu.org/copyleft/fdl.html).
