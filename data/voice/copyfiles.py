"""
DESCRIPTION:
Copies wav files from original directory to current directory (for git repos).
To convert wav to mp3, use the following bash script:

for f in *.wav; do lame $f $f.mp3; done

"""

import os
import shutil

src = "/home/lotje/Documents/PhD Marseille/Studies/008 - OVP in picture naming/008B/data/wav"
dst = "."

for pp in os.listdir(src):
	ppPath = os.path.join(src, pp, "vocal_response")
	
	os.makedirs(pp)
	
	for f in os.listdir(ppPath):
		if not "mp3" in f:
			continue
		
		_src = os.path.join(ppPath, f)
		_dst = os.path.join(dst, pp, f)
		
		shutil.copy(_src, _dst)
		
		